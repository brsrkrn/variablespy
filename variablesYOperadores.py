#Se importa el modulo de matematicas
import math

print "Programa que calcula las raices"
print "de una ecuacion de segundo grado.\n"

#Se piden y esperan los valores del usuario.
a = eval(raw_input("Dame el termnio cuadratico "))
b = eval(raw_input("Dame el termino lineal "))
c = eval(raw_input("Dame el termino independiente "))

#Se utilizan los valores ingresados por el usuario
#y se realizan los calculos necesarios para resolver el problema.
x1 = (-b+math.sqrt(math.pow(b,2)-4*a*c)) / (2*a);
x2 = (-b-math.sqrt(math.pow(b,2)-4*a*c)) / (2*a);

#Se muestran los resultados de los calculos evaluados
print "a  =",a
print "b  =",b
print "c  =",c
print "x1 =",x1
print "x2 =",x2

